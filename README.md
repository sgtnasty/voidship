# voidship

https://sgtnasty.com

Simulation of interplanetary naval engagements.

My new project (I'm going to "get" rust one way or another...)

## Goals

1. Learn rust, I mean really learn it for my go to lang for:
  a. system programming
  b. games
  c. GUI -> Gtk 4
2. Make a combat simulator: enter in parameters, belligerants, environment etc. and calc results
3. Have fun
