/*****************************************************************************
voidship
Copyright (C) 2022  Ronaldo Nascimento

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

extern crate rand;
use rand::{Rng, thread_rng};

#[derive(Debug)]
struct Point {
    x: f64,
    y: f64,
    z: f64
}

#[warn(dead_code)]
impl Point {
    fn new(x: f64, y: f64, z: f64) -> Point {
        Point{x:x, y:y, z:z}
    }

    fn origin() -> Point {
        Point { x: 0.0, y: 0.0, z: 0.0 }
    }
}

#[derive(Debug)]
struct Ship {
    location: Point,
    name: String
}

impl Ship {
    fn new(name: String) -> Ship {
        let p = Point::origin();
        return Ship{name:name,location:p}
    }
}

fn randomize_point<R: Rng + ?Sized>(rng: &mut R, p: &mut Point) {
    p.x = rng.gen();
    p.y = rng.gen();
    p.z = rng.gen();
}

fn main() {
    let mut rng = thread_rng();

    println!("voidship");
    println!("initializing encounter ...");    
    
    let mut s1: Ship = Ship::new("Fred A01".to_string());
    randomize_point(&mut rng, &mut s1.location);

    println!("Void ship `{}` is located at ({:0.2},{:0.2},{:0.2})", 
        s1.name, s1.location.x, s1.location.y, s1.location.z);
}
