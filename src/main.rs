/*****************************************************************************
voidship
Copyright (C) 2022  Ronaldo Nascimento

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

extern crate rand;

use rand::{Rng, thread_rng};

#[derive(Debug, Copy, Clone, PartialEq)]
struct Point {
    x: f64,
    y: f64,
    z: f64
}

#[warn(dead_code)]
impl Point {
    fn new(x: f64, y: f64, z: f64) -> Point {
        Point{x:x, y:y, z:z}
    }

    fn origin() -> Point {
        Point { x: 0.0, y: 0.0, z: 0.0 }
    }

    fn calc_distance(self, b: Point) -> f64 {
        let i = (b.x - self.x).powi(2);
        let j = (b.y - self.y).powi(2);
        let k = (b.z - self.z).powi(2);
        (i + j + k).sqrt()
    }
}

#[derive(Debug, Clone, PartialEq)]
struct Ship {
    location: Point,
    name: String,
    accel: f64,
    s_target: f64,
}

impl Ship {
    fn new(new_name: String) -> Ship {
        let p = Point::origin();
        Ship{name:new_name,location:p,accel:0.1,s_target:f64::MAX}
    }
}

fn randomize_location<R: Rng>(rng: &mut R, p: &mut Point) {
    p.x = rng.gen();
    p.y = rng.gen();
    p.z = rng.gen();
}

fn main() {
    let mut rng = thread_rng();
    let mut ships: Vec<Ship> = Vec::new();

    println!("voidship");
    println!("initializing encounter ...");    
    
    ships.push(Ship::new("Fred A01".to_string()));
    ships.push(Ship::new("Mary B01".to_string()));
    ships.push(Ship::new("Alex C01".to_string()));
    ships.push(Ship::new("Gary D01".to_string()));

    for i in 0..ships.len() {
        println!("{}", ships[i].name)
    }

    for i in 0..ships.len() {
        randomize_location(&mut rng, &mut ships[i].location);
        println!("{:?}", ships[i].location)
    }

    // now calculate distance to shortest target
    for i in 0..ships.len() {
        for j in 0..ships.len() {
            if ships[i].name != ships[j].name {
                let d = ships[i].location.calc_distance(ships[j].location);
                if d < ships[i].s_target {
                    ships[i].s_target = d;
                }
                println!("{} targets {} = {}", 
                    ships[i].name, ships[j].name, d);
            }
        }
    }
}
